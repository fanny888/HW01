package com.tgl.assignment;

import java.util.Stack;

public class homework9910 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println(isValid("{}[]"));
		System.out.println(isValid("{[}]"));
		System.out.println(isValid("[{}]"));

	}
	
	public static boolean isValid(String s) {
		char arr[] = s.toCharArray();
		Stack <Character> set = new Stack();
		for (Character ch : arr) {
			if (ch == '{' || ch == '[' || ch == '(') {
				set.push(ch);
			} else if (ch == ']') {
				if (set.isEmpty() || set.peek() != '[') {
					return false;
				} 
				set.pop();
			} else if (ch == ')') {
				if (set.isEmpty() ||  set.peek() != '(') 
					return false;
				set.pop();	
			} else if (ch == '}') {
				if (set.isEmpty() ||  set.peek() != '{') 
					return false;
				set.pop();	
			}
			
		}
		return set.size() == 0;
	}


}
