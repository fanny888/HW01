package com.tgl.assignment;

import java.util.*;
public class homework9904 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
/*
 Count the number of prime numbers less than a non-negative number, n.
 Example:
 Input: 10 Output: 4 
 Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
 */
		Scanner scanner = new Scanner(System.in); 
        int x=0;
        int count=0;
        System.out.println("Input:");
        int num = scanner.nextInt();
        int[] arr = new int[num];
        
        for (int i=2;i<=num;i++) {
            count = 0;
            for (int j=2;j<=i;j++) {
                 if (0==i%j)
                     count++;
            }
            if (count < 2) { 
                 arr[x] = i;
                 x++;
            }
        }   
        System.out.println("Output:"+x);
        System.out.print("Prime numbers :");
        for (int i=0;i<arr.length;i++)  // ??? array length 
        System.out.print(arr[i]);

		
	}

}
