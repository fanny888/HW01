package com.tgl.assignment;

import java.util.*;
public class homework9903 {
	public static void main(String[] args) {
		 /* Given a string s consists of upper/lower-case alphabets and empty space characters ' ', 
	       return the length of last word in the string. If the last word does not exist, return 0. 		
		   Note: A word is defined as a character sequence consists of non-space characters only.
			Example: Input: "Hello World"  Output: 5	
			*/
			 //  String str = "Hello World";
	        Scanner inputStr = new Scanner(System.in);        
	        System.out.println("input:");  
	        String strs=inputStr.nextLine();
	        String[] arrStr = strs.split(" ");
	        System.out.println("last word length:"+arrStr[arrStr.length-1].length());
	        System.out.println("last word is :"+arrStr[arrStr.length-1]);
	        System.out.println("word number:"+arrStr.length);
	       /* Console result 
	        input:
	        	Hello World
	        	last word length:5
	        	last word is :World
	        	*/
		}

		
	
}
