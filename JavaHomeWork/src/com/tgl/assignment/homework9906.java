package com.tgl.assignment;

import java.util.*;

public class homework9906 {

	public static void main(String[] args) {	
    /*
     Given a string s consists of upper/lower-case alphabets and empty space characters ' ', return the length of last word in the string. 
	 If the last word does not exist, return 0.
	 Note: A word is defined as a character sequence consists of non-space characters only.
	 Example: Input: "Hello World" Output: 5
     */
	 //  String str = "Happy every day";
        Scanner input = new Scanner(System.in);        
        System.out.println("input:");  
        String str=input.nextLine();
        String[] arr = str.split(" ");
        System.out.println("last word length:"+arr[arr.length-1].length());
        System.out.println("last word is :"+arr[arr.length-1]);

		
	}

}
