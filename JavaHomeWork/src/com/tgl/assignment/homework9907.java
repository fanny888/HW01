package com.tgl.assignment;

public class homework9907 {

	public static void main(String[] args) {
		/* Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.
		Note: For the purpose of this problem, we define empty string as valid palindrome.
		Example 1: Input: "A man, a plan, a canal: Panama"  Output: true
		Example 2: Input: "race a car" 		Output: false
       */

	 	String s = "A man, a plan, a canal: Panama" ;
	 	//String s = "race a car" ;
	 	
	 	boolean tf = true ; 
		
		   if(s==null){
	         tf =  false;
		    }
		 
		    s = s.toLowerCase();
		    s = s.trim() ;
		    int i=0;
		    int j=s.length()-1;
		 
		    while(i<j){
		        while(i<j && !((s.charAt(i)>='a' && s.charAt(i)<='z') 
		                    || (s.charAt(i)>='0'&&s.charAt(i)<='9'))){
		            i++;
		        }
		 
		        while(i<j && !((s.charAt(j)>='a' && s.charAt(j)<='z') 
		                    || (s.charAt(j)>='0'&&s.charAt(j)<='9'))){
		            j--;
		        }
		 
		        if(s.charAt(i) != s.charAt(j)){
		            tf = false;
		        }
		 
		        i++;
		        j--;
		    }
		 
		    		
		System.out.printf("String: %s , result %b " ,  s  ,  tf);
		
	
	}
}
